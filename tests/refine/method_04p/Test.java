// Interface methods can be refined.
// .grammar: { Node; }
public class Test {
  public static void main(String[] args) {
    Interface node = new Node();
    node.toBeRefined();
  }
}
