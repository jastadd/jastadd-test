aspect Test63 {
  // There are several errors here.
  // First, A.b() is not declared circular, although it circularly depends on itself,
  // via other attributes: b -> c1 -> c2 -> a(1) -> b
  // This results in a runtime exception during evaluation.
  // However, the problem cannot be fixed by declaring b as circular.
  // This is because there is another error: a, b, c1 and c2 do not have values
  // on a finite lattice. Their values will grow and grow at evaluation,
  // and the circular evaluation will never terminate.

  syn int A.a(String name) circular [1] {
    if (a(1) >= 2) {
      return 3;
    } else {
      return a(1) + 1;
    }
  }

  syn int A.a(int i) circular  [1]  {
    return a("x") + b();
  }

  syn int A.b() = c1();

  syn int A.c1() circular [0] = c2();
  syn int A.c2() circular [0] = c1() + a(1);
}
